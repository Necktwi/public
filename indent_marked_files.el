;;; package --- Summary
;;; Commentary:
;;; Code:

(require 'dired)
(defvar message-window nil)
(defvar dired-window)
(defvar dired-buffer)
(defvar dired-marked-files)
(defvar indent-remaining-files-flag)
(defvar dired-marked-files-set-flag)
(defvar total-files)
(defvar quit-char-input)
(defvar current-file-index)
(defvar remaining-files-to-indent)

(defun indent_marked_files ()
  "Format marked files with the indentation rules."
  (interactive)
  (setq dired-window (selected-window))
  (message "dired-window: %s" dired-window)
  (setq message-window nil)
  (when (not (windowp (setq message-window (get-buffer-window "*Messages*"))))
    (message "Splitting for a *Messages* window.")
    (split-window-below)
    (select-window (setq message-window (next-window)))
    (message "missage-window: %s" (selected-window))
    (switch-to-buffer "*Messages*"))
  (select-window dired-window)
  (setq dired-buffer (buffer-name))
  (message "dired-window: %s" (selected-window))
  (setq dired-marked-files (dired-get-marked-files))
  (setq dired-marked-files-set-flag nil)
  (setq remaining-files-to-indent nil)
  (when (file-exists-p "remaining-files-to-indent.el")
    (catch 'loop
      (while (not dired-marked-files-set-flag)
        (setq indent-remaining-files-flag
          (read-char "A remaining-files-to-indent.el list is found; Want to continue with it [y/n]? "))
        (pcase indent-remaining-files-flag
          (?y (setq remaining-files-to-indent
                    (read (get-string-from-file "remaining-files-to-indent.el")))
           (setq dired-marked-files remaining-files-to-indent)
           (setq dired-marked-files-set-flag t))
          (?n (throw 'loop t))
          (indent-remaining-files-flag nil)))))
  (message "Formatting marked files with indentation rules:")
  (setq total-files (length dired-marked-files))
  (setq current-file-index 0)
  (setq quit-char-input nil)
  (delete_all_hooks)
  (catch 'loop
    (dolist (file dired-marked-files)
      (setq current-file-index (1+ current-file-index))
      (message "(%d/%d) %s" current-file-index total-files file)
      (find-file file)
      (indent-region (point-min) (point-max))
      (save-buffer)
      (kill-buffer)
      (when (and (setq quit-char-input (read-char "q to stop indenting"
                                                  nil 1))
                 (or (eq quit-char-input ?q)
                     (eq quit-char-input ?Q)))
        (message "User interrupted.")
        (sit-for 1)
        (throw 'loop t))))
  (message "Writing remaing-files-to-indent.el...")
  (find-file "remaining-files-to-indent.el")
  (erase-buffer)
  (setq remaining-files-to-indent (nthcdr current-file-index
                                          dired-marked-files))
  (print remaining-files-to-indent (current-buffer))
  (save-buffer)
  (add_all_hooks)
  (message "done!"))

(defun get-string-from-file (filePath)
  "Return FILEPATH's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))
(defun delete_all_hooks ()
  "Delete all unnecessary hooks."
  (remove-hook 'c-mode-common-hook 'cmake_ide_init)
  (remove-hook 'cmake-mode-hook 'cmake_ide_init)
  (remove-hook 'c-mode-common-hook 'show-paren-mode)
  (remove-hook 'c-mode-common-hook 'autopair-mode)
  (remove-hook 'visual-line-mode-hook 'adaptive-wrap-prefix-mode)
  );(remove-hook 'after-init-hook #'global-flycheck-mode))
(defun add_all_hooks ()
  "Add back all deleted hooks."
  (add-hook 'c-mode-common-hook 'cmake_ide_init)
  (add-hook 'cmake-mode-hook 'cmake_ide_init)
  (add-hook 'c-mode-common-hook 'show-paren-mode)
  (add-hook 'c-mode-common-hook 'autopair-mode)
  (add-hook 'visual-line-mode-hook 'adaptive-wrap-prefix-mode)
  );(add-hook 'after-init-hook #'global-flycheck-mode))

(provide 'indent_marked_files)
;;; indent_marked_files.el ends here
